import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TradeInputFormComponent } from './trade-input-form.component';

describe('TradeInputFormComponent', () => {
  let component: TradeInputFormComponent;
  let fixture: ComponentFixture<TradeInputFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TradeInputFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TradeInputFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
