import { Component, OnInit } from '@angular/core';
import { TraderService } from '../services/trader.service';
import { Stock } from '../model/stock';
import { Strategy } from '../model/strategy';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ModalDirective} from 'angular-bootstrap-md';

@Component({
  selector: 'app-trade-input-form',
  templateUrl: './trade-input-form.component.html',
  styleUrls: ['./trade-input-form.component.css']
})
export class TradeInputFormComponent implements OnInit {

  // user input fields:
  ticker: string;
  numOfStocks: number;
  exitProfitLoss: number;

  stocks: Stock[];
  selectedStock: Stock;

  lowercaseStocks: Stock[] = [];

  basicModal: ModalDirective;
  newTicker: string;

  constructor(private traderService: TraderService) { }

  saveTraderInput() {
    console.log('ticker ' + this.ticker);
    console.log('numOfStocks ' + this.numOfStocks);
    console.log('maxLoss ' + this.exitProfitLoss);

    if (this.exitProfitLoss != null && this.numOfStocks != null && this.selectedStock != null) {
      this.saveStrategy();
    } else {
      console.log('Missing field');
    }
  }

  saveStock() {
    console.log('ticker ' + this.newTicker);

    this.traderService.saveStock(new Stock(-1, this.newTicker)).subscribe(
      newStock => {
        console.log('Stock was created: ' + newStock);
        this.traderService.newStock(newStock);
      },
      error => {
        console.log('Failed to save stock');
        console.log(error);
      }
    );
    location.reload();
  }

  saveStrategy() {
    console.log('selectedStock ' + this.selectedStock.id);

    this.traderService.saveStrategy(new Strategy(this.exitProfitLoss, -1, this.numOfStocks, this.selectedStock, 0, 0))
    .subscribe(
      newStrategy => {
        console.log('Strategy was created: ' + newStrategy);
        this.traderService.newStrategy(newStrategy);
      },
      error => {
        console.log('Failed to save strategy');
        console.log(error);
      }
    );

    location.reload();
  }

  ngOnInit() {
    // printing all stock ticker in the select box.
    this.traderService.getStock().subscribe(
      allStocks => {
        console.log('Received stocks: ' + allStocks);
        this.stocks = allStocks;
        // to show only the lower case ticker.
        this.stocks.forEach(
          s => {
            if (s.ticker === s.ticker.toLowerCase()){
              this.lowercaseStocks.push(s);
            }
          }
        );
      },
      error => {
        console.log('Unable to retrieve stocks');
      }
    );
  }

  onChange(event) {
    if (event.target.value === '++ create new stock ++') {
      document.getElementById('openModalButton').click();
    }
  }
}
