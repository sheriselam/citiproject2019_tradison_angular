import { Component, OnInit, ViewChild } from '@angular/core';
import { TraderService } from '../services/trader.service';
import { Strategy } from '../model/strategy';
import { TradeRecordTableComponent } from '../trade-record-table/trade-record-table.component';
import { TrendChartComponent } from '../trend-chart/trend-chart.component';

@Component({
  selector: 'app-strategy-table',
  templateUrl: './strategy-table.component.html',
  styleUrls: ['./strategy-table.component.css']
})
export class StrategyTableComponent implements OnInit {

  strategies: Strategy[];

  descStraList: Strategy[] = [];

  @ViewChild('tradeTable', {static: false}) tradeTable: TradeRecordTableComponent;

  @ViewChild('trendChart', {static: false}) trendChart: TrendChartComponent;
  dataToShow: Strategy;

  constructor(private traderService: TraderService) { }

  selectedStrategy: Strategy;

  setClickedRow(s: Strategy) {
    console.log('Strategy clicked: ' + s.id);
    this.selectedStrategy = s;
    this.tradeTable.tradeFilter(this.selectedStrategy);
  }

  stopStrategy(s: Strategy, stopped) {
    if (!stopped) {
      console.log('Button Clicked: ' + s.id + '; s.stopped: ' + stopped);
      this.traderService.stopStrategy(s).subscribe(
        data => {
          console.log('result of stop: ');
          console.log(data);
        },
        error => {
          console.log('An error occured');
          console.log(error);
        }
      );
      location.reload();
    }
  }

  openTrendTable(s: Strategy) {
    console.log('link is clicked ' + s.stock.ticker);
    // this.trendTable.dataToShow = s;
    this.trendChart.selectedData(s);
    // document.getElementById('openTrendChart').click();
  }

  ngOnInit() {
    this.traderService.getStrategies().subscribe(
      data => {
        console.log('Component Received Strategies from Backend!');
        console.log(data);
        this.strategies = data;
        // making the list to show in a decending order: i.e. the latest one come up first, so it will be easier to read.
        this.strategies = this.strategies.sort((a, b) => b.id - a.id);
      },
      error => {
        console.log('An error occured');
        console.log(error);
        alert('Failed to retrieve strategies: ' + error.message);
      }
    );
  }

}
