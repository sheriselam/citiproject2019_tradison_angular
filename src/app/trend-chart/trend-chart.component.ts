import { Component, OnInit, Input } from '@angular/core';
import { Strategy } from '../model/strategy';
import { TraderService } from '../services/trader.service';
import { formatDate } from '@angular/common';
import { AverageAndPriceHistory } from '../model/average-and-price-history';

@Component({
  selector: 'app-trend-chart',
  templateUrl: './trend-chart.component.html',
  styleUrls: ['./trend-chart.component.css']
})
export class TrendChartComponent implements OnInit {

  dataToShow: Strategy;

  // allTrades: Trade[] = [];
  // filteredTrades: Trade[] = [];

  averageAndPriceHistory: AverageAndPriceHistory[] = [];

  longAvg: number[] = [];
  shortAvg: number[] = [];
  prices: number[] = [];

  recordedDate: string[] = [];

  headerInfo: string;

  constructor(private traderService: TraderService) { }

  public chartType: string = 'line';

  public chartDatasets: Array<any> = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Long Average' },
    { data: [28, 48, 40, 19, 86, 27, 90], label: 'Short Average' },
    { data: [60, 70, 40, 20, 50, 80, 75], label: 'Price' }
  ];

  public chartLabels: Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];

  public chartColors: Array<any> = [
    {
      backgroundColor: 'rgba(105, 0, 132, .2)',
      borderColor: 'rgba(200, 99, 132, .7)',
      borderWidth: 2,
    },
    {
      backgroundColor: 'rgba(0, 137, 132, .2)',
      borderColor: 'rgba(0, 10, 130, .7)',
      borderWidth: 2,
    }
  ];

  public chartOptions: any = {
    responsive: true,
    scales: {
      xAxes: [{
        scaleLabel: {
          display: true,
          labelString: 'Time'
        }
      }],
      yAxes: [{
        scaleLabel: {
          display: true,
          labelString: 'Price'
        }
      }]
    },
  };
  public chartClicked(e: any): void { }
  public chartHovered(e: any): void { }

  ngOnInit() {
    console.log('testing chart component');
  }

  selectedData(s: Strategy) {
    this.dataToShow = s;
    console.log('dataToShow: ' + this.dataToShow.stock.ticker);

    this.headerInfo = 'Strategy ID: ' + this.dataToShow.id + ' with Stock Ticker: ' + this.dataToShow.stock.ticker;

    document.getElementById('openTrendChart').click();

    // this.getFiltered();
  }

  showDatasets() {
    this.traderService.getAaphByID(this.dataToShow).subscribe(
      data => {
        console.log('Component Received Trade from Backend!');
        console.log(data);
        this.averageAndPriceHistory = data;
        this.getFiltered(data);
        // document.getElementById('openTrendChart').click();
        console.log(this.chartDatasets);
        this.updateChartTimeLabels();
      },
      error => {
        console.log('An error occured');
        console.log(error);
        alert('Failed to retrieve trades: ' + error.message);
      }
    );
  }

  getFiltered(averageAndPriceHistory: any) {
    console.log('in filtered');

    // this.filteredTrades = [];
    this.longAvg = [];
    this.shortAvg = [];
    this.prices = [];
    this.recordedDate = [];

    this.averageAndPriceHistory.forEach(a => {
        this.longAvg.push(a.longAvg);
        this.shortAvg.push(a.shortAvg);
        this.prices.push(a.price);
        this.recordedDate.push(formatDate(a.recordDate, 'yy/MM/dd hh:mm:ss', 'en-uk', ''));
    });

    this.chartDatasets = [];
    this.chartDatasets = [
      { data: this.longAvg, label: 'Long Average' },
      { data: this.shortAvg, label: 'Short Average' },
      { data: this.prices, label: 'Price' },
    ];
  }

  updateChartTimeLabels() {
    this.chartLabels = [];
    this.recordedDate.forEach(sc => {
      this.chartLabels.push(sc);
    });
  }

  // TODO: refresh table element every 5 seconds.
}
