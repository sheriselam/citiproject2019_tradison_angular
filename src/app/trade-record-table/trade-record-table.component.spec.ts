import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TradeRecordTableComponent } from './trade-record-table.component';

describe('TradeRecordTableComponent', () => {
  let component: TradeRecordTableComponent;
  let fixture: ComponentFixture<TradeRecordTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TradeRecordTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TradeRecordTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
