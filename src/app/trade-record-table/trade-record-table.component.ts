import { Component, OnInit, Input } from '@angular/core';
import { Trade } from '../model/trade';
import { TraderService } from '../services/trader.service';
import { Strategy } from '../model/strategy';

@Component({
  selector: 'app-trade-record-table',
  templateUrl: './trade-record-table.component.html',
  styleUrls: ['./trade-record-table.component.css']
})
export class TradeRecordTableComponent implements OnInit {

  trades: Trade[];

  filteredTrades: Trade[] = [];

  constructor(private traderService: TraderService) { }

  ngOnInit() {
    this.traderService.getTrade().subscribe(
      data => {
        console.log('Component Received Trade from Backend!');
        console.log(data);
        this.trades = data;
      },
      error => {
        console.log('An error occured');
        console.log(error);
        alert('Failed to retrieve trades: ' + error.message);
      }
    );
  }

  tradeFilter(s: Strategy) {
    this.filteredTrades = [];

    this.trades.forEach(t => {
      if(t.strategy.id === s.id) {
        this.filteredTrades.push(t);
      }
    });
  }
}
