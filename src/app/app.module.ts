import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
// import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TradeInputFormComponent } from './trade-input-form/trade-input-form.component';
import { TradeRecordTableComponent } from './trade-record-table/trade-record-table.component';
import { HttpClientModule } from '@angular/common/http';
import { StrategyTableComponent } from './strategy-table/strategy-table.component';
import { TrendChartComponent } from './trend-chart/trend-chart.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

@NgModule({
  declarations: [
    AppComponent,
    TradeInputFormComponent,
    TradeRecordTableComponent,
    StrategyTableComponent,
    TrendChartComponent
  ],
  imports: [
    BrowserModule,
    // AppRoutingModule,
    FormsModule,
    HttpClientModule,
    MDBBootstrapModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
