import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Stock } from '../model/stock';
import { Strategy } from '../model/strategy';
import { Trade } from '../model/trade';
import { AverageAndPriceHistory } from '../model/average-and-price-history';

@Injectable({
  providedIn: 'root'
})
export class TraderService {

  private newStockSource = new Subject<Stock>();
  newStockObservable = this.newStockSource.asObservable();

  private newStrategySource = new Subject<Strategy>();
  newStrategyObservable = this.newStrategySource.asObservable();

  constructor(private httpClient: HttpClient) { }

  // For Stocks
  public getStock(): Observable<Stock[]> {
    return this.httpClient.get<Stock[]>(environment.backendUrl + '/api/v1/stocks');
  }

  newStock(stock: Stock) {
    this.newStockSource.next(stock);
  }

  saveStock(stock: Stock): Observable<Stock> {
    return this.httpClient.post<Stock>(
      environment.backendUrl + '/api/v1/stocks', stock,
      {headers: new HttpHeaders({'Content-Type': 'application/json'})}
    );
  }

  // For Strategies
  public getStrategies(): Observable<Strategy[]> {
    return this.httpClient.get<Strategy[]>(environment.backendUrl + '/api/v1/strategies');
  }

  newStrategy(strategy: Strategy) {
    this.newStrategySource.next(strategy);
  }

  saveStrategy(strategy: Strategy): Observable<Strategy> {
    return this.httpClient.post<Strategy>(
      environment.backendUrl + '/api/v1/strategies', strategy,
      {headers: new HttpHeaders({'Content-Type': 'application/json'})}
    );
  }
  stopStrategy(strategy: Strategy): Observable<Strategy> {
    console.log('in Method:' + strategy.id);
    console.log(environment.backendUrl + '/api/v1/strategies/stop/' + strategy.id);
    return this.httpClient.get<Strategy>(environment.backendUrl + '/api/v1/strategies/stop/' + strategy.id);
  }

  // For Trade
  public getTrade(): Observable<Trade[]> {
    return this.httpClient.get<Trade[]>(environment.backendUrl + '/api/v1/trades');
  }

  // For Average And Price History
  public getAverageAndPriceHistory(): Observable<AverageAndPriceHistory[]> {
    return this.httpClient.get<AverageAndPriceHistory[]>(environment.backendUrl + '/api/v1/averageAndPriceHistory');
  }

  public getAaphByID(strategy: Strategy): Observable<AverageAndPriceHistory[]> {
    return this.httpClient.get<AverageAndPriceHistory[]>(environment.backendUrl + '/api/v1/averageAndPriceHistory/' + strategy.id);
  }
}
