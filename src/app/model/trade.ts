import { Strategy } from './strategy';

export class Trade {
    constructor(public stockTicker: string, public price: number,
                public strategy: Strategy, public tradeType: string, public lastStateChange: string,
                public longAvg: number, public shortAvg: number) {}
}
