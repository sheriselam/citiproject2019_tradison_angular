import { Stock } from './stock';

export class Strategy {
    constructor(public exitProfitLoss: number, public id, public size: number, public stock: Stock,
                public longAverage: number, public shortAverage: number) {}
}
// , public maxProfit: number, public exitDate: string,

// {
//     "currentPosition": 0,
//     "exitProfitLoss": 0,
//     "id": 0,
//     "lastTradePrice": 0,
//     "profit": 0, -- current profit
//     "size": 0, -- number of stock
//     "stock": {
//       "id": 0,
//       "ticker": "string"
//     }
//   }