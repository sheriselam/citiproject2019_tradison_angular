import { Strategy } from './strategy';

export class AverageAndPriceHistory {

    constructor(public price: number, public longAvg: number, public shortAvg: number,
                public strategy: Strategy, public recordDate: string) {}
}
