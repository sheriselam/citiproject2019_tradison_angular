FROM nginxinc/nginx-unprivileged:latest

EXPOSE 8080

COPY dist/et-platform/* /usr/share/nginx/html/
